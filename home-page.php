<?php 

/**
 * Template Name: Home Page
 */

get_header();

get_template_part('partials/home', 'banner');

get_template_part('partials/home', 'boxes');

get_template_part('partials/home', 'courses');

get_template_part('partials/home', 'buttons');

get_footer();

?>