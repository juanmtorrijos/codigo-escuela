<?php get_header();

get_template_part( 'partials/single', 'title' );

get_template_part( 'partials/single', 'content' );

get_template_part( 'partials/about_author' );

get_footer(); ?>