<div class="courses">
	<?php if (is_front_page()) : ?>

		<h2>Nuestros Cursos</h2>

	<?php endif; ?>

	<div class="courses-section level-1">
		<div class="course-text">
			<h3>Desarrollo Web Nivel 1:</h3>

			<h4>Aprende a Construir una página web</h4>

			<p>En este curso aprenderás las bases de HTML versión 5 y CSS versión 3. Aprenderás a posicionar elementos en la página utilizando CSS para así crear diseños sofisticados. Aprenderás a usar la herramienta de desarrollo de Google Chrome para depurar tu código. Finalmente aprenderás a subir tus archivos a un servidor de alojamiento web (web hosting) y apuntar tu nombre de dominio a tu sitio web.</p>

			<a href="<?php echo home_url('/cursos/desarrollo-web-nivel-1/') ?>">Conoce Más de este curso »</a>

		</div>
	</div>

	<div class="courses-section level-2">
		<div class="course-text">
			<h3>Desarrollo Web Nivel 2:</h3>

			<h4>Aprende CSS a fondo</h4>

			<p>En este curso aprenderás las técnicas más avanzadas en CSS, el lenguaje que le da estilos y posiciona los elementos en tu página web. Aprenderás técnicas para adaptar la distribución de los diseños de acuerdo al tamaño de la ventana convirtiendo la página web en una página responsiva. Aprenderás técnicas para darle animación a los elementos en tu página web con animaciones en CSS.</p>

			<a href="<?php echo home_url('/cursos/desarrollo-web-nivel-2/') ?>">Conoce Más de este curso »</a>

		</div>
	</div>

	<div class="courses-section level-3">
		<div class="course-text">
			<h3>Desarrollo Web Nivel 3:</h3>

			<h4>Aprende JavaScript y jQuery</h4>

			<p>En este curso aprenderás los conceptos básicos del lenguaje de programación de la web y uno de los más importantes de la era contemporánea. Aprenderás a manipular los elementos en tu página web y crearás páginas web más dinámicas. Utilizarás la librería jQuery para crear Banners animados, ampliadores de imágenes (lightboxes), formularios con validación, esconder y mostrar elementos y más.</p>
		</div>

		<a href="<?php echo home_url('/cursos/desarrollo-web-nivel-3/') ?>">Conoce Más de este curso »</a>
		
	</div>
</div>