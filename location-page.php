<?php 

/**
 * Template Name: Location Page
 */

get_header();

get_template_part( 'partials/page', 'title' );

get_template_part( 'partials/page', 'map' );

get_template_part( 'partials/page', 'content' );

get_footer(); ?>
