<div class="page-title">
	<div class="main-content">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<h1><?php the_title(); ?></h1>
		<?php endwhile; endif; wp_reset_query(); ?>
	</div>
</div>