<?php
/**
 * Define Theme root and image path with a constant
 */
define ( 'THEMEROOT', get_template_directory_uri() );
define ( 'IMAGESPATH', THEMEROOT . '/images' );

setlocale(LC_ALL, "es_ES");

/**
 * Register styles and scripts the WordPress way
 */
function register_my_styles_and_scripts() {
	
	wp_register_style( 'main_style', THEMEROOT . '/css/main.css', array(), '2016', 'all' );
	wp_enqueue_style( 'main_style' );

	if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
    wpcf7_enqueue_scripts();
	}

	if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
	    wpcf7_enqueue_styles();
	}

	if ( is_page_template( 'home-page.php' )) {
		wp_enqueue_script( 'typedjs', THEMEROOT . '/js/typed.js', array('jquery'), '2016', true );
		wp_enqueue_script( 'homepagejs', THEMEROOT . '/js/home-page.js', array('jquery'), '2016', true );
	}

	if ( is_page_template( 'location-page.php' ) ) {
		wp_enqueue_script('googlemaps_js', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCG_OCZXmdBDBfK7rdHX_O9dX2LI2IAM5c', array(), '', false);

		wp_enqueue_script( 'google_map_js', THEMEROOT . '/js/google-maps.js', array(), '2016', true  );

	}

	wp_enqueue_script( 'main_js', THEMEROOT . '/js/min/main-min.js', array('jquery'), '2016', true );
	
}

add_action( 'wp_enqueue_scripts', 'register_my_styles_and_scripts' );

require get_template_directory() . '/includes/google-fonts.php';
require get_template_directory() . '/includes/add-gotcha-attribute-to-form.php';
require get_template_directory() . '/includes/add_validation_for_mailchimp.php';
require get_template_directory() . '/includes/google-analytics-tracking.php';
require get_template_directory() . '/includes/wp-title-hack-for-home.php';
require get_template_directory() . '/includes/excerpt-more.php';
require get_template_directory() . '/includes/contact-form-7-customization.php';
require get_template_directory() . '/includes/html5shimie9.php';
require get_template_directory() . '/includes/js-class-body.php';
require get_template_directory() . '/includes/post-thumb.php';
require get_template_directory() . '/includes/fontawesome.php';
require get_template_directory() . '/includes/register-sidebars.php';
require get_template_directory() . '/includes/register-menus.php';
require get_template_directory() . '/includes/add-local-remote-button-admin-bar.php';
require get_template_directory() . '/includes/add-twitter-field-to-author.php';
require get_template_directory() . '/post-types/cursos.php';
require get_template_directory() . '/post-types/acf-code.php';