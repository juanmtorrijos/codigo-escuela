<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="course-page-title">
	<div class="course-page-content">

		<h1><?php the_title(); ?>: <span class="subtitulo"><?php the_field('sub_titulo'); ?></span></h1>

		<p>En este curso aprenderás:</p>
		
		<div class="aprenderas">
			<?php the_field('aprenderas'); ?>
		</div>

	</div>
</div>

<?php endwhile; endif; wp_reset_query(); ?>