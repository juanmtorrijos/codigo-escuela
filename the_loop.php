<div class="articles">
    <div class="main-content">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<article>
		<h1><?php the_title(); ?></h1>
		<?php the_excerpt(); ?>
	</article>

<?php endwhile; ?>

<?php else: ?>

    <article>
        <h1>¡Hola!</h1>
        <p>Aún no has publicado un artículo. Si quieres publicar un artículo has <a href="<?php echo admin_url( 'post-new.php' ); ?>">click aquí</a>.</p>
    </article>

<?php endif; ?>

    </div> <!-- #main -->
</div> <!-- #main-container -->