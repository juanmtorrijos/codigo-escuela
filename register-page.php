<?php

/**
 * Template Name: Register Form Page
 */

get_header();

get_template_part( 'partials/page', 'title' );

get_template_part('partials/register_form');

get_footer(); ?>