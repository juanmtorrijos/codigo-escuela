<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="<?php echo bloginfo('language');?>"> <!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php bloginfo( 'name' ); ?><?php wp_title('|'); ?></title>
    <meta name="description" content="<?php echo bloginfo('description'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >

<div class="header-container">
    <header class="logo-navigation">
        <div class="logo">
            <a href="<?php echo home_url(); ?>">
                <img src="<?php echo IMAGESPATH; ?>/logo-codigoescuela.png" alt="Logo Código Escuela">
            </a>
        </div>

        <?php get_template_part( 'partials/header', 'nav' ); ?>

        <?php get_template_part( 'partials/header', 'login' ); ?>
    </header>
</div>