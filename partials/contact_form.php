<div class="main-content">

	<div class="contact-form-content">
		
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 
		the_content();
		endwhile; endif; wp_reset_query(); ?>

	</div>

</div>