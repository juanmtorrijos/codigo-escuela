<?php

/**
 * Template Name: Contact Form Page
 */

get_header();

get_template_part( 'partials/page', 'title' );

get_template_part('partials/contact_form');

get_footer(); ?>