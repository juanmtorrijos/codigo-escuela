<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php if( get_field('contenido_de_curso') ) : ?>
	
	<h3 class="titulo-contenido">Temas del Curso: </h3>

	<div class="contenido-de-curso">

	<?php if ( have_rows('contenido_de_curso') ) : while( have_rows('contenido_de_curso') ) : the_row(); ?>

		<div class="seccion-contenido">
			
			<?php echo get_sub_field('section_number'); ?>

		</div>

	<?php endwhile; endif; ?>

	</div>

	<?php endif; ?>

<?php endwhile; endif; wp_reset_query(); ?>