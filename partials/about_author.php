<div class="author">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="author-gravatar">
		
		<?php echo get_avatar( get_the_author_meta( 'ID' ), 120  ); ?>

	</div>
	<div class="about-author">
		<h3>Acerca del Autor</h3>
		
		<p><?php echo get_the_author_meta( 'description' ); ?></p>

	</div>
	<?php endwhile; endif; wp_reset_query(); ?>
</div>