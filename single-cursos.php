<?php 

/**
 * Single Course Page
 */

get_header();

get_template_part( 'partials/course_page', 'title' );

get_template_part( 'partials/course_page', 'schedule' );

get_template_part( 'partials/page', 'incluidos' );

get_template_part( 'partials/course_page', 'content' );

get_footer();

?>