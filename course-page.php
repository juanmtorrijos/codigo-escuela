<?php 

/**
 * Template Name: Courses Page
 */

get_header();

get_template_part( 'partials/page', 'title' );

get_template_part( 'partials/home', 'courses' );

get_template_part( 'partials/page', 'incluidos' );

get_footer();

?>