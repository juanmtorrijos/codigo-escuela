
(function initMap() {
  var map;
  
  var googleMapsPMTS = 'https://www.google.com.pa/maps/place/Panama+Maritime+Training+Services/@8.9925986,-79.5018829,18z/data=!4m6!1m3!3m2!1s0x8faca90850e33171:0x9fe354a9eee42acd!2sPanama+Maritime+Training+Services!3m1!1s0x8faca90850e33171:0x9fe354a9eee42acd?hl=es';

  var myLatLng = { lat: 8.9927923, lng: -79.5018624 };

  var mapCenter = {
    lat: 9,
    lng: -79.501
  };

  var zoom = 14;

  map = new google.maps.Map(document.getElementById('map-canvas'), {
    center: mapCenter,
    zoom: zoom,
    scrollwheel: false,
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    animation: google.maps.Animation.DROP,
    map: map,
    title: 'Código Escuela'
  });

  var contentString =   '<div id="map-content" class="map-content">';
      contentString +=  '<h2>Código Escuela</h2>';
      contentString +=  '<p>Estamos ubicados en Calle 77 San Francisco.<br>';
      contentString +=  'En el Edificio Panama Maritime Training Services, Inc.<br>';
      contentString +=  'Entrando por la Casa del Helado y el Banco Banistmo.<br>';
      contentString +=  'Después del Rey, al Final de Calle 50.<br>';
      contentString +=  'Llámenos al +(507) 395-2802 para más información</p>'
      contentString +=  '<p><a href="' + googleMapsPMTS + '" target="_blank" >Búscanos en Google Maps</a></p>';
      contentString +=   '</div>';


  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });

})();