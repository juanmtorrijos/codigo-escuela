; (function( $ ) {
$(function() {

window.setTimeout(function(){

	var typedCursor = $('span.typed-cursor'),
		spanEndCode = $('span.end-code'),
		$window = $(window);

	var humanizeType = Math.round((Math.random() * 10) + 20);

	function backspace() {
	  var word = spanEndCode.html();
	  var wordLength = word.length;
	  var newWordLength = wordLength - 1;
	  var newWord = word.slice(0, newWordLength);
	  spanEndCode.html(newWord);
	  if (newWordLength === 0) {
	    clearInterval(intrvl);
	  } else {
	    newWordLength = wordLength;
	  }
	}


	function hideCursor() {
		typedCursor.hide();
	}

	function typeIt() {
		spanEndCode.typed({
			cursorChar: "_",
			backDelay: 800,
			typeDelay: 500,
			strings: [
				"Blogs",
				"Aplicaciones Web",
				"Comunidades Online", 
				"Negocios Online",
			],
		});
	}

	if ($window.width() > 800) {
		var intrvl = setInterval(backspace, humanizeType);
		window.setTimeout( hideCursor, 1000 );
		window.setTimeout( typeIt, 1250 );
	}

}, 1500);


});
})(jQuery);