<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="single-title" 
<?php if (has_post_thumbnail()) : ?>
	style="background: url('<?php the_post_thumbnail_url(); ?>') top center no-repeat; background-size: cover;"
<?php endif; ?>
>
	<div class="main-content">
			<h1
				<?php if (has_post_thumbnail()) : ?>
					style="text-shadow: 0 0 8px black; margin: 12em 0 2em;"
				<?php endif; ?>
			><?php the_title(); ?></h1>
	</div>
</div>
<?php endwhile; endif; wp_reset_query(); ?>