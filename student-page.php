<?php  

/**
 * Template Name: Student Logged In Page
 */

if ( is_user_logged_in() ) {

	get_header();

	get_template_part( 'partials/page', 'title' );

	get_template_part( 'partials/page', 'content' );

	get_footer();


} else {

	wp_redirect( home_url() );

}

?>