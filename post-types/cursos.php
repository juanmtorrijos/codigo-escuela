<?php

function cursos_init() {
	register_post_type( 'cursos', array(
		'labels'            => array(
			'name'                => __( 'Cursos', 'codigoescuela' ),
			'singular_name'       => __( 'Curso', 'codigoescuela' ),
			'all_items'           => __( 'Todos los Cursos', 'codigoescuela' ),
			'new_item'            => __( 'Nuevo Curso', 'codigoescuela' ),
			'add_new'             => __( 'Añadir Nuevo', 'codigoescuela' ),
			'add_new_item'        => __( 'Añadir Nuevo Curso', 'codigoescuela' ),
			'edit_item'           => __( 'Editar Cursos', 'codigoescuela' ),
			'view_item'           => __( 'Ver Curso', 'codigoescuela' ),
			'search_items'        => __( 'Buscar Cursos', 'codigoescuela' ),
			'not_found'           => __( 'No se encontraron cursos', 'codigoescuela' ),
			'not_found_in_trash'  => __( 'No se econtraron cursos en la papelera', 'codigoescuela' ),
			'parent_item_colon'   => __( 'Cursos Padre', 'codigoescuela' ),
			'menu_name'           => __( 'Cursos', 'codigoescuela' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-welcome-learn-more',
	) );

}
add_action( 'init', 'cursos_init' );

function cursos_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['cursos'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Curso actualizado. <a target="_blank" href="%s">Ver Curso</a>', 'codigoescuela'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'codigoescuela'),
		3 => __('Custom field deleted.', 'codigoescuela'),
		4 => __('Curso actualizado.', 'codigoescuela'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Curso restaurado a revisión desde %s', 'codigoescuela'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Curso publicado. <a href="%s">Ver Curso</a>', 'codigoescuela'), esc_url( $permalink ) ),
		7 => __('Curso guardado.', 'codigoescuela'),
		8 => sprintf( __('Curso enviado. <a target="_blank" href="%s">Previsualizar Curso</a>', 'codigoescuela'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Curso programado para: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Previsualizar Curso</a>', 'codigoescuela'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Curso borrador actualizado. <a target="_blank" href="%s">Previsualizar Curso</a>', 'codigoescuela'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'cursos_updated_messages' );
