<?php

/**
 * Prints Google fonts link to the head
 * 
 */
function register_google_fonts() {
	echo "<link href='https://fonts.googleapis.com/css?family=Lora:300,400,700|Nunito:400,700|Josefin+Sans:300' rel='stylesheet' type='text/css'>";
}
add_action('wp_head', 'register_google_fonts');