<?php 

/**
 * The Blog Page
 */

get_header();

get_template_part( 'partials/blog', 'title' );

get_template_part( 'partials/news_articles' );

get_footer();