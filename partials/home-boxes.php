<div class="home-boxes">
	<div class="box hacer-paginas-web">
		<div class="box-image"></div>
		<h2>Aprende a Hacer Páginas Web</h2>
		<p>Aprende a escribir los lenguajes pilares de la web: HTML, CSS y JavaScript y toma control de los sitios que diseñas.</p>
	</div>
	<div class="box paginas-responsivas">
		<div class="box-image"></div>
		<h2>Aprende a crear Páginas Web Responsivas</h2>
		<p>Aprende a crear páginas web que se adapten al tamaño de cualquier dispositivo en donde son vistas: teléfonos inteligentes, tablets, laptops y desktops.</p>
	</div>
	<div class="box paginas-interactivas">
		<div class="box-image"></div>
		<h2>Aprende a crear Páginas Web Interactivas</h2>
		<p>Aprende a utilizar las más importantes librerías, plataformas y procesadores de HTML, CSS y JavaScript para crear páginas web dinámicas e interactivas.</p>
	</div>
</div>