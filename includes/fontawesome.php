<?php

/**
 * Link to Font Awesome and place it in the footer
 * Only for single news page
 * 
 */

  function register_fontawesome() {
    if ( is_single() ) {
  	echo '<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">';
    }
  }
  add_action('wp_footer', 'register_fontawesome');