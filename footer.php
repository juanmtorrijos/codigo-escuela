<div class="footer-container">
    <footer class="main-content">
        <div class="footer-logo-box">
        	<h3>Código Escuela</h3>
        	<p>Código Escuela es un sitio para aprender a crear páginas web ubicada en la ciudad de Panamá.</p>
        </div>
        <div class="footer-box">
        	<h3>Contáctenos</h3>
        	<ul>
        		<li class="email">cursos@codigoescuela.com</li>
        		<li class="phone"><a href="tel:395-2802">+(507)395-2802</a></li>
        	</ul>
        </div>
        <div class="footer-box">
        	<h3>Ubicación</h3>
        	<ul>
        		<li>Calle 77, San Francisco,</li>
        		<li>Edificio #29</li>
        		<li>Ciudad de Panamá,</li>
        		<li><a href="#0">Ver en Mapa</a></li>
        	</ul>
        </div>
        <div class="footer-box">
        	<?php get_template_part( 'partials/footer', 'register_form' ); ?>
        </div>
    </footer>
</div>
<div class="footer-copyright">
	<p>Código Escuela &copy; 2014 - <?php echo date( 'Y' ); ?>. Todos los derechos Reservados.</p>
</div>
<?php wp_footer(); ?>
</body>
</html>