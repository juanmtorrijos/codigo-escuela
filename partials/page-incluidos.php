<div class="page-incluidos">
	<div class="incluidos">
		<div class="incluidos-image instructor"></div>
		<h2>Cursos Prescenciales</h2>
		<p>Los cursos se dicatarán en salones equipados con computadoras de escritorio con las aplicaciones necesarias para empezar tu aprendizaje de manera eficaz. Adicionalmente contarás con la participación de un instructor experimentado a tu entera disposición.</p>
	</div>
	<div class="incluidos">
		<div class="incluidos-image libro"></div>
		<h2>Libro de Guía</h2>
		<p>los cursos incluye un libro para ser utilizado como guía. Estos libros constan de la descripción de los conceptos básicos tocados durante el curso, al igual que una gran variedad de ejemplos, los cuales te servirán de base para tus primeros proyectos.</p>
	</div>
	<div class="incluidos">
		<div class="incluidos-image acceso-web"></div>
		<h2>Acceso a Materiales vía Web</h2>
		<p>Se te proporcionarán credenciales personalizadas para que accedas a la plataforma Web  para así obtengas las presentaciónes y los libros de cada curso actualizado con las últimas tendencias de la materia.</p>
	</div>
</div>