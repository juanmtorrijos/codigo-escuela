<?php if ( have_posts() ) : while ( have_posts() ) : the_post();

$duracion_de_curso = get_field('duracion_de_curso');
$fecha_de_inicio = get_field('fecha_de_inicio');
$fecha_final = get_field('fecha_final');

?>

<div class="schedule-pricing">

	<div class="fechas-horarios">
		<div class="imagen-fecha-horarios"></div>
		<h2>Fechas y Horarios</h2>
		<div class="fecha">
			<div class="fecha-inicio">
				<h3><?php echo date('d', strtotime($fecha_de_inicio)); ?></h3>
				<p class="mes"><?php echo ucwords(strftime('%B', strtotime($fecha_de_inicio))); ?></p>
			</div>
			<div class="fecha-final">
				<h3><?php echo date('d', strtotime($fecha_final)); ?></h3>
				<p class="mes"><?php echo ucwords(strftime('%B', strtotime($fecha_final))); ?></p>
			</div>
			<p>
				Lunes a Viernes de 6pm a 9pm<br>
				Sábado de 9am a 2pm
			</p>
			<p class="small">Duración de Curso: <?php echo $duracion_de_curso; ?> horas</p>
		</div>
	</div>

	<div class="precio-curso">
		<div class="imagen-precio-curso"></div>
		<h2>Precio</h2>
		<p class="descuento">$180.00</p>
		<p class="precio-real">$360.00</p>
		<a href="<?php echo home_url( 'registrate' ); ?>" class="main-button">Inscríbete</a>
	</div>
</div>

<?php endwhile; endif; wp_reset_query(); ?>