<div class="login-button">
	<?php if (!is_user_logged_in()) : ?>
		<a href="<?php echo home_url( '/login' ); ?>" class="login-button-header">Iniciar Sesión</a>
	<?php else : ?>
		<a href="<?php echo wp_logout(); ?>" class="login-button-header">Salir</a>
	<?php endif; ?>
</div>