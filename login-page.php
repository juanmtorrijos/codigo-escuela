<?php 

/**
 * Template Name: Login Page
 */

if ( ! is_user_logged_in() ) {

	get_header();

	get_template_part( 'partials/page', 'title' );

	echo('<div class="login-page">');

	get_template_part( 'plugins/login-with-ajax/widget_out' );

	get_template_part( 'partials/login', 'content' );

	echo('</div>');

	get_footer();

} else {

	wp_redirect( home_url( 'pagina-estudiantes' ) );

}

?>