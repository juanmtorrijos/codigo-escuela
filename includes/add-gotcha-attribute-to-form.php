<?php

function add_gotcha_attribute() {

	if (is_page_template( 'contact-page.php' )) {
		echo "
		<script>
	        var pendingCourse = document.getElementById('pending-course');
	        pendingCourse.setAttribute('name', '_gotcha');
	    </script>
		";
	}

}

add_action( 'wp_footer', 'add_gotcha_attribute' );