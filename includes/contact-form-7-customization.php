<?php
/**
 * Contact Form 7
 * Loading Javascript only when it is necessary
 */
	add_filter( 'wpcf7_load_js', '__return_false' );
	add_filter( 'wpcf7_load_css', '__return_false' );

/**
 * Custom Ajax Loader
 * 
 */
add_filter('wpcf7_ajax_loader', 'my_wpcf7_ajax_loader');
function my_wpcf7_ajax_loader () {
	return  get_bloginfo('stylesheet_directory') . '/images/contactform-loader.gif';
}