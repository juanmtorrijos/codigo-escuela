<div class="login-form">
	<form id="login" action="login" method="post">
		<span class="status"></span>
		<label for="your-username">
			<p class="short">Username</p>
			<input type="text" placeholder="Your Username" id="username">
		</label>
		<label for="your-password">
			<p class="short">Password</p>
			<input type="password" id="password">
		</label>
		<div class="submit-button">
			<input type="submit" value="Login" name="submit">
		</div>
		<a class="lost" href="<?php echo wp_lostpassword_url(); ?>">Lost your password?</a>
	</form>
</div>