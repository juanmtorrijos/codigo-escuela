<div class="news-articles">
	<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>
	<article>
		<?php if ( has_post_thumbnail() ) : ?>
		<figure class="post-thumb">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( array(522, 600) ); ?>
			</a>
		</figure>
		<?php endif; ?>

		<div class="news">
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<div class="date-author">
				<?php 
					the_date( 'd F \d\e Y', '<p>Publicado el ', ' Escrito por ' ); 
					the_author();
					echo '</p>';
				?>
			</div>
			<?php the_excerpt(); ?>
		</div>
	</article>
	<?php endwhile; else: get_template_part('templates/404_message'); endif; wp_reset_query(); ?>
</div>