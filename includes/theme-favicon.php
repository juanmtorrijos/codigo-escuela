<?php

/**
 * Prints favicon link to the head
 * 
 */
function register_theme_favicon() {
  echo '<link rel="shortcut icon" href="' . THEMEROOT . '/favicon.ico" />';
}
add_action( 'wp_head', 'register_theme_favicon' );