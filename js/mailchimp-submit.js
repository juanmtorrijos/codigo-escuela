; (function( $ ) {
$(function() {

var $mailChimpForm = $('form#mc-embedded-subscribe-form'),
	$h3Boletin = $('h3.boletin-footer'),
	$thanks = $('p.thanks');

function displayThanks(){
	$mailChimpForm.fadeOut();
	$thanks.fadeIn();
	$h3Boletin.html('¡Gracias!');
}

$mailChimpForm.on('submit', displayThanks);

});
})(jQuery);