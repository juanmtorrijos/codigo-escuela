<div class="home-banner">
	<div class="messages-buttons">
		<h1 class="primary-message">
			<span class="code-init">>_</span>
			<span class="start-code">Aprende</span>
			<span class="middle-code">a Crear</span>
			<span class="end-code">Páginas Web</span><span class="typed-cursor">_</span>
		</h1>

		<h2 class="secondary-message">Conviértete en un Desarrollador Web.</h2>

		<?php get_template_part('partials/buttons'); ?>
	</div>
</div>